-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 07-12-2020 a las 21:25:50
-- Versión del servidor: 10.1.25-MariaDB
-- Versión de PHP: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `gestorventas`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `nombre` varchar(50) NOT NULL,
  `apellido` varchar(50) NOT NULL,
  `telefono` int(11) NOT NULL,
  `identificacion` int(11) NOT NULL,
  `fechaingreso` varchar(30) NOT NULL,
  `rol` varchar(30) NOT NULL,
  `usuario` varchar(50) NOT NULL,
  `contrasena` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`nombre`, `apellido`, `telefono`, `identificacion`, `fechaingreso`, `rol`, `usuario`, `contrasena`) VALUES
('juan pablo', 'ortega', 2938973, 1, '7/12/2020', 'Administrador', 'adm', '1234'),
('julian', 'marin', 103, 30, '4/12/2020', 'Administrador', 'ff', '12'),
('hj', 'wew', 23, 111, '2/111/2020', 'Vendedor', 'qws', '11'),
('william', 'ortega', 4555, 123, '28/5/2017', 'Vendedor', 'jj', '1234'),
('juan', 'ortiz', 344, 890, '2020/11/4', 'Vendedor', 'rr', '123'),
('nestor', 'ortiz', 1234, 1234, '2020/11/3', 'Vendedor', 'hhh', '1234'),
('carlos', 'ortega', 2938973, 4555, '2020/11/12', 'Vendedor', 'lll', '1234'),
('julio', 'martin', 123, 9867, '4/11/2020', 'Vendedor', '123', '123'),
('jua', 'coca', 12334, 12390, '2/11/2020', 'Vendedor', '1122', '123'),
('Daniel', 'marin', 123, 45161, '4/11/2020', 'Vendedor', '123', '123'),
('juan ortega', 'borquez', 30076978, 55555, '2/12/2020', 'Vendedor', 'yy', '123'),
('juanp', 'cristancho', 123, 451610, '4/11/2020', 'Vendedor', 'nnn', '123'),
('bv', 'ddd', 123, 4516102, '4/11/2020', 'Vendedor', '11', '123'),
('orlando', 'arias', 1233, 45161027, '5/11/2020', 'Vendedor', 'nnn', 'hhh'),
('juan pablo ', 'ortega cristancho', 300769601, 1015999090, '5/12/2020', 'Vendedor', 'kkkk', '12345'),
('Daniel ', 'Zabala', 123, 1030574098, '4/11/2020', 'Vendedor', '1w', '123');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`identificacion`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
