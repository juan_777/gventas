/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestiondeventas;

/**
 *
 * @author user
 */
import java.sql.*;
import javax.swing.JOptionPane;

public class MyDBConnection {
    private Connection myConnection;
    /** Creates a new instance of MyDBConnection */
    public MyDBConnection() {
    }
   public void init(){
     try{
        Class.forName("org.gjt.mm.mysql.Driver").newInstance();
        myConnection=DriverManager.getConnection(
"jdbc:mysql://localhost/gestorventas","root", ""
                );
        //JOptionPane.showMessageDialog(null,"Connected");
        }
        catch(Exception e){
            JOptionPane.showMessageDialog(null,"Disconnected");
            e.printStackTrace();
        }
    }
   public Connection getMyConnection(){
        return myConnection;
    }
    public void close(ResultSet rs){
       if(rs !=null){
            try{
               rs.close();
            }
            catch(Exception e){}
       }
    }
     public void close(java.sql.Statement stmt){
       if(stmt !=null){
            try{
               stmt.close();
            }
            catch(Exception e){}
       }
    }
     
  public void destroy(){
   if(myConnection !=null){
        try{
               myConnection.close();
            }
            catch(Exception e){}
   }
  }  
}
