/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestiondeventas;

import java.sql.ResultSet;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author user
 */
public class AddArticulos extends javax.swing.JFrame {

    
    Conexion_SQL cc = new Conexion_SQL();
    com.mysql.jdbc.Connection con = cc.conexion();
    /**
     * Creates new form AddArticulos
     */
    public AddArticulos() {
        initComponents();
        MostrarDatosArticulos();
        MostrarDatosArticulosadd(Productos_JP.txtcodigoproducto.getText());
        this.setLocationRelativeTo(null);
    }
    //Productos_JP pro=new Productos_JP();
    
    public void AddArticuloaproduc(String articulo, String Ud ,String IdArticulo){         
        
     try {
         
         String cantidad=txtcantidad.getText();
         String cantidad1=cantidad.concat("-"+Ud);
            String SQL= "insert into detalleproducto (codigo,nombrep,nombrea,cantidad,IdArticulo) values (?,?,?,?,?)";
            java.sql.PreparedStatement pst= con.prepareStatement(SQL);
            
            pst.setString(1, Productos_JP.txtcodigoproducto.getText());
            pst.setString(2,Productos_JP.txtnombreproducto.getText());
            pst.setString(3,articulo);
            pst.setString(4,cantidad1);
            pst.setString(5,IdArticulo);
            
            pst.execute();
            
            JOptionPane.showMessageDialog(null,"El articulo se agrego al producto");
             } catch (Exception e) {
            JOptionPane.showMessageDialog(null,"El articulo no se agrego error " + e.getMessage());
            }
    
    } 
public void MostrarDatosArticulos(){
    
        String[] titulos = {"ID","Nombre","Descripcion","Unidad de medida"};
        String[] registros = new String[4];
        DefaultTableModel Model = new DefaultTableModel(null,titulos);
        String SQL= "select * from articulo";
        
        try {
            java.sql.Statement st= con.createStatement();
            ResultSet rs = st.executeQuery(SQL);
            
            while (rs.next()){
                registros[0]=rs.getString("IDarticulo");
                registros[1]=rs.getString("nombre");
                registros[2]=rs.getString("descripcion");
                registros[3]=rs.getString("medida");
                Model.addRow(registros);
            }
            tablaAmostrar.setModel(Model);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,"no se pueden mostrar los articulos error" + e);
        }
    }
public void MostrarDatosArticulosadd(String codigo){
        //String criterio=Productos_JP.txtcodigoproducto.getText();
        if(codigo.equals("")){}else{
        String[] titulos = {"ID","Codígo","Nombre Producto","Nombre Artículo","Cantidad"};
        String[] registros = new String[5];
        DefaultTableModel Model = new DefaultTableModel(null,titulos);
        
         String SQL="select * from detalleproducto where codigo like '%"+codigo+"%'";
        
        try {
            java.sql.Statement st= con.createStatement();
            ResultSet rs = st.executeQuery(SQL);
            
            while (rs.next()){
                registros[0]=rs.getString("IDdp");
                registros[1]=rs.getString("codigo");
                registros[2]=rs.getString("nombrep");
                registros[3]=rs.getString("nombrea");
                registros[4]=rs.getString("cantidad");
                Model.addRow(registros);
            }
            tablaaddp.setModel(Model);
            
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,"no se pueden mostrar los articulos error" + e);
        }
        }
    }
 public void Eliminar(){
    int filaseleccionada= tablaaddp.getSelectedRow();
        if (filaseleccionada==-1){
        JOptionPane.showMessageDialog(this, "Seleccione un artículo de la tabla detalle producto");
        }else{
                
        try {
        String SQL="delete from detalleproducto where IDdp=?";
        
        java.sql.PreparedStatement st= con.clientPrepareStatement(SQL);
        
        st.setString(1, tablaaddp.getValueAt(filaseleccionada,0).toString());
        
        int n= st.executeUpdate();
            
        // executeUpdate se usa para acciones que modifican una DB, y retorna el numero de filas afectadas por la accion
            if (n>=0){
                JOptionPane.showMessageDialog(null,"Se elimino el articulo del sistema");
            }
           
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,"Error al intentar eliminar el articulo del sistema" + e.getMessage());
        } 
        }
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tablaAmostrar = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        tablaaddp = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jButton4 = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel3 = new javax.swing.JLabel();
        txtcantidad = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        tablaAmostrar.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(tablaAmostrar);

        tablaaddp.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane2.setViewportView(tablaaddp);

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/if_button_cancel_1939.png"))); // NOI18N
        jButton1.setText("Eliminar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/if_xfce-system-exit_23651.png"))); // NOI18N
        jButton2.setText("Cerrar");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jLabel1.setText("Tabla Artículos");

        jLabel2.setText("Tabla Artículos añadidos");

        jButton4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/if_add_101239.png"))); // NOI18N
        jButton4.setText("Insertar");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        jLabel3.setText("Cantidad:");

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel4.setText("Agrega artículos ");

        jLabel5.setText("Nota: Seleccione un articulo y coloque la cantidad, acontinuación inserte el artículo");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(19, 19, 19)
                        .addComponent(jSeparator1))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addContainerGap(412, Short.MAX_VALUE)
                                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addContainerGap(75, Short.MAX_VALUE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel2)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                        .addGroup(layout.createSequentialGroup()
                                            .addGap(24, 24, 24)
                                            .addComponent(jLabel3)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                            .addComponent(txtcantidad, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(jButton4))
                                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                            .addComponent(jLabel1)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 103, Short.MAX_VALUE)
                                            .addComponent(jLabel5))
                                        .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.LEADING)))))
                        .addGap(0, 13, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addGap(41, 41, 41)
                .addComponent(jLabel4)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(28, 28, 28)
                        .addComponent(jButton4))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtcantidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3))))
                .addGap(18, 18, 18)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 42, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        
        String CodigoProducto = Productos_JP.txtcodigoproducto.getText();
        String NombreProducto = Productos_JP.txtnombreproducto.getText();
        String Cantidad = txtcantidad.getText();
        
        if("".equals(Productos_JP.txtcodigoproducto.getText())||"".equals(Productos_JP.txtnombreproducto.getText())||"".equals(txtcantidad.getText()) ){

            //Mensaje por si no se llenaron todos los campos necesarios
            JOptionPane.showMessageDialog(this, "Revisar los campos de Codígo y Nombre de Producto, tambien el campo de Cantidad de Artículo, por favor");
        }
        else{int filaseleccionada= tablaAmostrar.getSelectedRow();
        if (filaseleccionada==-1){
            JOptionPane.showMessageDialog(this, "Seleccione un artículo de la tabla Artículos");
        }else{
        String IdArticulo=(String) tablaAmostrar.getValueAt(filaseleccionada,0);    
        String nombrea= (String) tablaAmostrar.getValueAt(filaseleccionada,1);
        String umedida= (String) tablaAmostrar.getValueAt(filaseleccionada,3);
        AddArticuloaproduc(nombrea, umedida,IdArticulo);
        MostrarDatosArticulosadd(Productos_JP.txtcodigoproducto.getText());
        }
        }
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        int filaseleccionada= tablaaddp.getSelectedRow();
        if (filaseleccionada==-1){
            JOptionPane.showMessageDialog(this, "Seleccione un artículo de la tabla Artículos añadidos");
        }else{
        
        Eliminar();
        MostrarDatosArticulosadd(Productos_JP.txtcodigoproducto.getText());
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
       
        Productos_JP.ActArtcOfProduct.doClick();
        this.dispose();
    }//GEN-LAST:event_jButton2ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(AddArticulos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(AddArticulos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(AddArticulos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(AddArticulos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new AddArticulos().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton4;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTable tablaAmostrar;
    public static javax.swing.JTable tablaaddp;
    private javax.swing.JTextField txtcantidad;
    // End of variables declaration//GEN-END:variables
}
