/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestiondeventas;

import java.sql.ResultSet;
import java.util.Arrays;
import javax.swing.JOptionPane;

/**
 *
 * @author dzabala13
 */
public class ValidacionInventario {
    
    Conexion_SQL cc = new Conexion_SQL();
    com.mysql.jdbc.Connection con = cc.conexion();
    
    
    
     public  void ValidacionStockProductos(){
     
     // se obtienen todos los productos de la tabla productos         
    String SQL1="select * from productos"; 
    
     try {
         java.sql.Statement st= con.createStatement();
         ResultSet rs = st.executeQuery(SQL1);
         
         while (rs.next()){
            
             // se obtiene el codigo de cada producto
            String codigo = rs.getString("Codigo");  
            
//             System.out.println(codigo+"\n");
             
             try {
                 // para cada producto se revisan los articulos que lo componen
                 String SQL2="select * from detalleproducto where codigo = '"+codigo+"'";  
                 java.sql.Statement st2= con.createStatement();
                 ResultSet rs2 = st2.executeQuery(SQL2);
                 int Bandera=0;
                 
                 while(rs2.next()){
                     // se obtiene el id de cada articulo de  producto i
                     String codigoArticulo = rs2.getString("IdArticulo");  
                     int  CantidadArticulo = obtenercantidad(rs2.getString("cantidad"));
                     
                     try {     
                           // se realiza la busqueda de ese articulo dentro de la tabla "articulo"
                           String SQL3="select * from articulo where IDarticulo = '"+codigoArticulo+"'"; 
                           java.sql.Statement st3= con.createStatement();
                           ResultSet rs3 = st3.executeQuery(SQL3);
                      
                           while(rs3.next()){
                               // se valida que todos los articulos del producto cuenten con el stock suficiente.
                               String auxiliar="true";
                               String StockArticulo=rs3.getString("existencias");
                               
//                               System.out.println(codigoArticulo+" el  total de existencias son "+ StockArticulo);
                               
                                // se estable la condicion para validar que  haya la cantiada necesaria de cada articulo
                               if (Integer.parseInt(StockArticulo)>= CantidadArticulo && Bandera==0) {
                                   auxiliar="true";
                               }else{
                                   auxiliar="false";
                                   Bandera=1;
                               } 
//                                  System.out.println(auxiliar + " "+ Bandera);
                                  
                                  //Actualizacion de la disponibilidad de cada uno de los productos
                                   try {
                                   String SQL4 ="update productos set Disponibilidad=? where Codigo=?";
                                   java.sql.PreparedStatement pst4= con.prepareStatement(SQL4);
                                  
                                   pst4.setString(1, auxiliar);
                                   pst4.setString(2, codigo);
                                   
                                   pst4.executeUpdate();
                                   
                               } catch (Exception e) {
                                   JOptionPane.showMessageDialog(null,"se presento un error al actualizar la disponibilidad del producto" +e.getMessage());
                               }
                           }
    
                     } catch (Exception e) {
                         JOptionPane.showMessageDialog(null,"se presento un error al hacer la validacion de cada articulo" +e.getMessage());
                     }     
                 }
                 
//                 System.out.println("\n");
                 
             } catch (Exception e) {
                 JOptionPane.showMessageDialog(null,"se presento un error al buscar dentro de la tabla detalleproducto" + e.getMessage());
             }
         }
     } catch (Exception e) {
         JOptionPane.showMessageDialog(null," se presento un error al validar todos los productos existentes "+  e.getMessage());
     }
 }
     
         public  void ActualizacionStockProductos(String codigo){
 
             // se obtiene el codigo de cada producto
//            String codigo = "1948";  
//             System.out.println(codigo+"\n");
             try {
                 // para cada producto se revisan los articulos que lo componen
                 String SQL2="select * from detalleproducto where codigo = '"+codigo+"'";  
                 java.sql.Statement st2= con.createStatement();
                 ResultSet rs2 = st2.executeQuery(SQL2);
                 int Bandera=0;
                 
                 while(rs2.next()){
                     // se obtiene el id de cada articulo de  producto i
                     String codigoArticulo = rs2.getString("IdArticulo");  
                     int  CantidadArticulo = obtenercantidad(rs2.getString("cantidad"));
                     
                     try {     
                           // se realiza la busqueda de ese articulo dentro de la tabla "articulo"
                           String SQL3="select * from articulo where IDarticulo = '"+codigoArticulo+"'"; 
                           java.sql.Statement st3= con.createStatement();
                           ResultSet rs3 = st3.executeQuery(SQL3);
                      
                           while(rs3.next()){
                               // se valida que todos los articulos del producto cuenten con el stock suficiente.
                               int StockArticulo=Integer.parseInt(rs3.getString("existencias"));
                               
                               int NuevoStock=StockArticulo-CantidadArticulo;
                               
                                  
                                  //Actualizacion de la disponibilidad de cada uno de los productos
                                   try {
                                   String SQL4 ="update articulo set existencias=? where IDarticulo=?";
                                   java.sql.PreparedStatement pst4= con.prepareStatement(SQL4);
                                  
                                   pst4.setString(1, String.valueOf(NuevoStock));
                                   pst4.setString(2, codigoArticulo);
                                   
                                   pst4.executeUpdate();
                                   
                               } catch (Exception e) {
                                   JOptionPane.showMessageDialog(null,"se presento un error al  modificar el stock del producto de acuerdo a la venta, error en tabla articulo. " +e.getMessage());
                               }
                           }
    
                     } catch (Exception e) {
                         JOptionPane.showMessageDialog(null,"se presento un error al  buscar cada uno de los articulos relacionados con el producto en tabla articulo. " +e.getMessage());
                     }     
                 }
                 
//                 System.out.println("\n");
                 
             } catch (Exception e) {
                 JOptionPane.showMessageDialog(null,"se presento un error al acceder a la tabla detalles de producto por lo cual no se puede relizar la actualizacion del stock del producto" + e.getMessage());
             }
 }
  
  
     
public int obtenercantidad(String item ){

//String item="2-ud-Unidad";
String[] parts = item.split("-");              
//System.out.println(Arrays.asList(parts));
String cantidad = parts[0];
//System.out.println(datos);
int CantidadInt = Integer.parseInt(cantidad);

return CantidadInt;

}
         
}
