/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestiondeventas;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.sql.Blob;
import java.util.ArrayList;
import java.sql.ResultSet;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
/**
 *
 * @author user
 */
public class Venta extends javax.swing.JPanel {
    Conexion_SQL cc = new Conexion_SQL();
    com.mysql.jdbc.Connection con = cc.conexion();
    ValidacionInventario ActStock= new ValidacionInventario();
    /**
     * Creates new form Venta
     */
    public Venta() {
        initComponents();
        llenarcategoriaventa();
        ActStock.ValidacionStockProductos();
        mDatosTablaproducto();
        
        
        // esta parte de es la tabla de las ventas parciales se debe acomodar dentro de una funcion o dentro de una clase para que quede mas entendible 
        
        String[] titulos = {"Item","Cantidad","Descripcion","Precio unidad"};
        DefaultTableModel Model = new DefaultTableModel(null,titulos);
        TablaVentaParcial.setModel(Model);
        
    }
 public void llenarcategoriaventa(){
     cbxcategoriaventa.removeAllItems();
                
    ArrayList<String> list= new ArrayList<String>();
    String SQL="select * from categoria";
    
        try {
           
           java.sql.PreparedStatement st= con.clientPrepareStatement(SQL);
           ResultSet rs = st.executeQuery(SQL);
//        
            while(rs.next()){
            list.add(rs.getString("nombre"));
            
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,"Error al combox" + e.getMessage());
        }
        cbxcategoriaventa.addItem("--Seleccione--");
        for(int i=0; i<list.size(); i++){
            
            cbxcategoriaventa.addItem(list.get(i));
            
        }  
    }
 
 
 public  void ValidacionStockProductos(){
     
    String SQL1="select * from productos"; // se obtienen todos los productos de la tabla productos
    
     try {
         java.sql.Statement st= con.createStatement();
         ResultSet rs = st.executeQuery(SQL1);
         
         while (rs.next()){
             
            String codigo = rs.getString("Codigo");  // se obtiene el codigo de cada producto
            
//             System.out.println(codigo+"\n");
             
             try {
                 String SQL2="select * from detalleproducto where codigo = '"+codigo+"'";  // para cada producto se revisan los articulos que lo componen
                 java.sql.Statement st2= con.createStatement();
                 ResultSet rs2 = st2.executeQuery(SQL2);
                 int Bandera=0;
                 
                 while(rs2.next()){
                     String codigoArticulo = rs2.getString("IdArticulo");  // se obtiene el id de cada articulo de  producto i
 
                     try {
                         
                           String SQL3="select * from articulo where IDarticulo = '"+codigoArticulo+"'"; // se realiza la busqueda de ese articulo dentro de la tabla "articulo"
                           java.sql.Statement st3= con.createStatement();
                           ResultSet rs3 = st3.executeQuery(SQL3);
                      
                           while(rs3.next()){
                               // se valida que todos los articulos del producto cuenten con el stock suficiente.
                               String auxiliar="true";
                               String StockArticulo=rs3.getString("existencias");
                               
//                               System.out.println(codigoArticulo+" el  total de existencias son "+ StockArticulo);
                               
                               if (Integer.parseInt(StockArticulo)> 0 && Bandera==0) {
                                   auxiliar="true";
                               }else{
                                   auxiliar="false";
                                   Bandera=1;
                               } 
//                                  System.out.println(auxiliar + " "+ Bandera);
                                  
                                  //Actualizacion de la disponibilidad de cada uno de los productos
                                  
                                  try {
                                   String SQL4 ="update productos set Disponibilidad=? where Codigo=?";
                                   java.sql.PreparedStatement pst4= con.prepareStatement(SQL4);
                                  
                                   pst4.setString(1, auxiliar);
                                   pst4.setString(2, codigo);
                                   
                                   pst4.executeUpdate();
                                   
                               } catch (Exception e) {
                                   JOptionPane.showMessageDialog(null,"se presento un error al actualizar la disponibilidad del producto" +e.getMessage());
                               }
                           }
    
                     } catch (Exception e) {
                         JOptionPane.showMessageDialog(null,"se presento un error al hacer la validacion de cada articulo" +e.getMessage());
                     }     
                 }
                 
//                 System.out.println("\n");
                 
             } catch (Exception e) {
                 JOptionPane.showMessageDialog(null,"se presento un error al buscar dentro de la tabla detalleproducto" + e.getMessage());
             }
         }
     } catch (Exception e) {
         JOptionPane.showMessageDialog(null," se presento un error al validar todos los productos existentes "+  e.getMessage());
     }
 }
 
 
 public void mDatosTablaproducto(){
    
    tablaproductoventa.setDefaultRenderer(Object.class, new TablaImagen());
    DefaultTableModel modelo=new DefaultTableModel();
    
    modelo.addColumn("Codigo");
    modelo.addColumn("Nombre Producto");
    modelo.addColumn("Precio ($)");
    modelo.addColumn("Imagen");
       
    String SQL="select * from productos where Disponibilidad like 'true'";
    
    try {          
         java.sql.Statement st= con.createStatement();
         ResultSet rs = st.executeQuery(SQL);
  
        while(rs.next()){
            
            Object[] fila= new Object[4];
        
            fila[0]= rs.getObject(1);
            fila[1]= rs.getObject(2);    
            fila[2]= rs.getObject(6);  
            
            try {
                Blob  blob = rs.getBlob(3);
                byte[] data= blob.getBytes(1,(int) blob.length());
                BufferedImage img= null;
                img =ImageIO.read(new ByteArrayInputStream(data));
                ImageIcon icono = new ImageIcon(img.getScaledInstance(70, 70, 0));
                fila [3]= new JLabel(icono);
            } catch (Exception e) {
                fila [3]= "No Imagen";
            //JOptionPane.showMessageDialog(null,"no se pudo leer la imagen" + e.getMessage());
            }    
            modelo.addRow(fila);
        }
        tablaproductoventa.setModel(modelo);
        tablaproductoventa.setRowHeight(64);
        
    } catch (Exception e) {
        JOptionPane.showMessageDialog(null, "Error al mostrar los datos tabla"+ e.getMessage());
    }    
    
    }
 
 
 public void AddItemVenta(){
             DefaultTableModel ModelTable = (DefaultTableModel) TablaVentaParcial.getModel();    
        
        int filaseleccionada= tablaproductoventa.getSelectedRow();
         if (filaseleccionada==-1){
            JOptionPane.showMessageDialog(this, "Seleccione un pruducto para realizar la venta");
        }else{

            String CodigoProducto= Integer.toString( (int) tablaproductoventa.getValueAt(filaseleccionada,0));
            // falta acomodar la cantidad que realmente se va a vendar y enlazarlo con la funcion de actualizaciond e stock 
            // falta enlazar las ventas con la tabla de compras donde se muestran las existencias de cada uno de los productos
            // toca mostrar los articulos relacionados a cada productos en la interfaz de ventas para poder obtener la descripcion de cada uno de los productos
            // verificar muy bien la interfaz de los compras por que genera un error cuando se le ingresa la cantidad y el precio
            int Cantidad=  2; 
            int precio= (int) tablaproductoventa.getValueAt(filaseleccionada,2);
            
            ActStock.ActualizacionStockProductos(CodigoProducto);

            String[] registros = new String[4];

            registros[0]=CodigoProducto;
            registros[1]=Integer.toString(Cantidad);
            registros[2]="falta agregar los detalles de los pruductos";
            registros[3]=Integer.toString(precio);

            ModelTable.addRow(registros);       
            TablaVentaParcial.setModel(ModelTable);
         }
         
}
   
 
 public void filtrarDatos(String categoria){
    
    tablaproductoventa.setDefaultRenderer(Object.class, new TablaImagen());
    
    DefaultTableModel modelo=new DefaultTableModel();
    
    modelo.addColumn("Nombre Producto");
    modelo.addColumn("Precio ($)");
    modelo.addColumn("Imagen");
       
    String SQL="select * from productos where categoria like '%"+categoria+"%'";
    
    try {
        java.sql.Statement st= con.createStatement();
            
            ResultSet rs = st.executeQuery(SQL);

                
       while(rs.next()){
            
             Object[] fila= new Object[3];
        
            fila[0]= rs.getObject(2);
            fila[1]= rs.getObject(6);
            
            
            
            
            try {
                Blob  blob = rs.getBlob(3);
                byte[] data= blob.getBytes(1,(int) blob.length());
                BufferedImage img= null;
                img =ImageIO.read(new ByteArrayInputStream(data));
                ImageIcon icono = new ImageIcon(img.getScaledInstance(70, 70, 0));
                fila [2]= new JLabel(icono);
            } catch (Exception e) {
                fila [2]= "No Imagen";
            //JOptionPane.showMessageDialog(null,"no se pudo leer la imagen" + e.getMessage());
            }    
            modelo.addRow(fila);
        }
        tablaproductoventa.setModel(modelo);
        tablaproductoventa.setRowHeight(64);
        
    } catch (Exception e) {
        JOptionPane.showMessageDialog(null, "Error al mostrar los datos tabla"+ e.getMessage());
    }  
    
}        

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPopupMenu1 = new javax.swing.JPopupMenu();
        actcategventas = new javax.swing.JMenuItem();
        Acttablaproductoventa = new javax.swing.JMenuItem();
        jPanel1 = new javax.swing.JPanel();
        cbxcategoriaventa = new javax.swing.JComboBox<>();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaproductoventa = new javax.swing.JTable();
        jSpinner1 = new javax.swing.JSpinner();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jComboBox2 = new javax.swing.JComboBox<>();
        jLabel4 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        TablaVentaParcial = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        jTextField2 = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();

        actcategventas.setText("jMenuItem1");
        actcategventas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                actcategventasActionPerformed(evt);
            }
        });
        jPopupMenu1.add(actcategventas);

        Acttablaproductoventa.setText("jMenuItem1");
        Acttablaproductoventa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ActtablaproductoventaActionPerformed(evt);
            }
        });
        jPopupMenu1.add(Acttablaproductoventa);

        jPanel1.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        cbxcategoriaventa.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cbxcategoriaventa.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbxcategoriaventaItemStateChanged(evt);
            }
        });

        jLabel1.setText("Filtrar por Categoria:");

        tablaproductoventa.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tablaproductoventa);

        jLabel2.setText("Cantidad:");

        jLabel3.setText("Código:");

        jComboBox2.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jLabel4.setText("Agregar Pago:");

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/shopping-cart-add256_24821 (1).png"))); // NOI18N
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton5.setText("jButton5");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel3)
                            .addComponent(jLabel1))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(cbxcategoriaventa, 0, 170, Short.MAX_VALUE)
                            .addComponent(jTextField1))
                        .addGap(25, 25, 25))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jScrollPane1)
                        .addContainerGap())))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(0, 24, Short.MAX_VALUE)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jSpinner1, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel4)
                        .addGap(18, 18, 18)
                        .addComponent(jComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(121, 121, 121)
                        .addComponent(jButton5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addComponent(jButton2)
                .addGap(23, 23, 23))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbxcategoriaventa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addGap(22, 22, 22)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(27, 27, 27)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jSpinner1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel4)
                            .addComponent(jComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButton5)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        TablaVentaParcial.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        TablaVentaParcial.setComponentPopupMenu(jPopupMenu1);
        jScrollPane2.setViewportView(TablaVentaParcial);

        jPanel2.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel5.setText("Total a pagar");

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel6.setText("$");

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel5))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(41, 41, 41)
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel6)
                    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(33, 33, 33))
        );

        jPanel3.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel8.setText("Paga con:");

        jTextField2.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel9.setText("Cambio:");

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel11.setText("$");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTextField2)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel9)
                            .addComponent(jLabel8))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel11)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 33, Short.MAX_VALUE)
                        .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 254, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel9)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel11, javax.swing.GroupLayout.DEFAULT_SIZE, 39, Short.MAX_VALUE))
                .addGap(12, 12, 12))
        );

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/iconfinder_cashbox_45016.png"))); // NOI18N

        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/shopping-cart-remove256_24799.png"))); // NOI18N

        jButton4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/1486485557-add-create-new-more-plus_81188.png"))); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 676, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(76, 76, 76)
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jButton4, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(17, 17, 17))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 383, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(81, 81, 81))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 144, Short.MAX_VALUE)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, 109, Short.MAX_VALUE)
                        .addComponent(jButton3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void actcategventasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_actcategventasActionPerformed
        // TODO add your handling code here:
        
        
    }//GEN-LAST:event_actcategventasActionPerformed

    private void cbxcategoriaventaItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbxcategoriaventaItemStateChanged
        // TODO add your handling code here:
        String cat=(String)cbxcategoriaventa.getSelectedItem();
        filtrarDatos(cat);
    }//GEN-LAST:event_cbxcategoriaventaItemStateChanged

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
       
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        AddItemVenta();
       ActStock.ValidacionStockProductos();
        mDatosTablaproducto();
    }//GEN-LAST:event_jButton5ActionPerformed

    private void ActtablaproductoventaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ActtablaproductoventaActionPerformed
       mDatosTablaproducto(); // actualizacion de la tabla desde otros panels
    }//GEN-LAST:event_ActtablaproductoventaActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JMenuItem Acttablaproductoventa;
    private javax.swing.JTable TablaVentaParcial;
    private javax.swing.JMenuItem actcategventas;
    private javax.swing.JComboBox<String> cbxcategoriaventa;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JComboBox<String> jComboBox2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSpinner jSpinner1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JTable tablaproductoventa;
    // End of variables declaration//GEN-END:variables
}
