/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestiondeventas;

import java.util.ArrayList;
import java.sql.ResultSet;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
/**
 *
 * @author user
 */
public class Compra extends javax.swing.JPanel {

    Conexion_SQL cc = new Conexion_SQL();
    com.mysql.jdbc.Connection con = cc.conexion();
    ValidacionInventario ActStock= new ValidacionInventario();
    /**
     * Creates new form Compra
     */
    public Compra() {
        initComponents();
        llenararticuloscompra();
        llenarproveedorcompra();
        MostrarDatostabla();
       
    }

    public void llenararticuloscompra(){
     Cbxarticulo.removeAllItems();
                
    ArrayList<String> list= new ArrayList<String>();
    String SQL="select * from articulo";
    
        try {
           
           java.sql.PreparedStatement st= con.clientPrepareStatement(SQL);
           ResultSet rs = st.executeQuery(SQL);
//        
            while(rs.next()){
            list.add(rs.getString("nombre"));
            
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,"Error al combox" + e.getMessage());
        }
        for(int i=0; i<list.size(); i++){
        
            
            Cbxarticulo.addItem(list.get(i));
        }    
    }
    public void llenarproveedorcompra(){
    Cboxproveedor.removeAllItems();
                
    ArrayList<String> list= new ArrayList<String>();
    String SQL="select * from proveedores";
    
        try {
           
           java.sql.PreparedStatement st= con.clientPrepareStatement(SQL);
           ResultSet rs = st.executeQuery(SQL);
//        
            while(rs.next()){
            list.add(rs.getString("nombre"));
            
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,"Error al combox" + e.getMessage());
        }
        for(int i=0; i<list.size(); i++){
        
            
            Cboxproveedor.addItem(list.get(i));
        }
        
    }
    public void guardarcompra(){
    try {
            String SQL= "insert into compra (articulo,preciouni,cantidad,preciotot,comprobante,numero,asunto,proveedor,fecha) values (?,?,?,?,?,?,?,?,?)";
                   
//                    
                    
            java.sql.PreparedStatement pst= con.prepareStatement(SQL);
            
            pst.setString(1, Cbxarticulo.getSelectedItem().toString());
            pst.setString(2,txtprecioun.getText());
            pst.setString(3,txtcantidad.getText());
            pst.setString(4,Lbtotal.getText());
            pst.setString(5,jComboBox2.getSelectedItem().toString());
            pst.setString(6,txtnumero.getText());
            pst.setString(7,jComboBox3.getSelectedItem().toString());
            pst.setString(8,Cboxproveedor.getSelectedItem().toString());
            String fecha=FechaActual();
            pst.setString(9,fecha);      
            
            pst.execute();
            
            JOptionPane.showMessageDialog(null,"La compra se registro en el sistema");
            
             } catch (Exception e) {
            JOptionPane.showMessageDialog(null,"La compra no se registro error " + e.getMessage());
            }
    try {
            String sql= "update articulo set existencias=? where nombre=?";
            java.sql.PreparedStatement pst= con.prepareStatement(sql);
            pst.setString(1,txtcantidad.getText());
            pst.setString(2,Cbxarticulo.getSelectedItem().toString());
            pst.executeUpdate();
             
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,"La cantidad no se registro error " + e.getMessage());
        }
    }
    public static String FechaActual(){
        Date fechaDate = new Date();
        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
        return formato.format(fechaDate);
    }
    public void MostrarDatostabla(){
     String[] titulos = {"ID","Artículo","Existencias","Unidad de medida"};
        String[] registros = new String[4];
        DefaultTableModel Model = new DefaultTableModel(null,titulos);
        String SQL= "select * from articulo";
        
        try {
            java.sql.Statement st= con.createStatement();
            ResultSet rs = st.executeQuery(SQL);
            
            while (rs.next()){
                registros[0]=rs.getString("IDarticulo");
                registros[1]=rs.getString("nombre");
                registros[2]=rs.getString("existencias");
                registros[3]=rs.getString("medida");
                Model.addRow(registros);
            }
            tablaarticuloCompra.setModel(Model);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,"no se pueden mostrar los articulos error" + e);
        }
    }
    public void Limpiar(){
                txtprecioun.setText("");
                txtcantidad.setText("");
                Lbtotal.setText("");
                txtnumero.setText("");
                jComboBox2.setSelectedItem("--Seleccione--");
                jComboBox3.setSelectedItem("--Seleccione--");
    }       
    
    
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPopupMenu1 = new javax.swing.JPopupMenu();
        Articulos_Compra = new javax.swing.JMenuItem();
        proveedor_compra = new javax.swing.JMenuItem();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        Cbxarticulo = new javax.swing.JComboBox<>();
        jLabel2 = new javax.swing.JLabel();
        txtprecioun = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        Lbtotal = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtcantidad = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jComboBox2 = new javax.swing.JComboBox<>();
        jLabel6 = new javax.swing.JLabel();
        txtnumero = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jComboBox3 = new javax.swing.JComboBox<>();
        jLabel8 = new javax.swing.JLabel();
        Cboxproveedor = new javax.swing.JComboBox<>();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel10 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        lbmedida = new javax.swing.JLabel();
        lbmedida1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaarticuloCompra = new javax.swing.JTable();
        jLabel9 = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();

        Articulos_Compra.setText("Articulos_Compra");
        Articulos_Compra.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Articulos_CompraActionPerformed(evt);
            }
        });
        jPopupMenu1.add(Articulos_Compra);

        proveedor_compra.setText("jMenuItem1");
        proveedor_compra.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                proveedor_compraActionPerformed(evt);
            }
        });
        jPopupMenu1.add(proveedor_compra);

        jPanel1.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jLabel1.setText("Artículo:");

        Cbxarticulo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        Cbxarticulo.setComponentPopupMenu(jPopupMenu1);
        Cbxarticulo.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        Cbxarticulo.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                CbxarticuloItemStateChanged(evt);
            }
        });
        Cbxarticulo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                CbxarticuloMouseClicked(evt);
            }
        });
        Cbxarticulo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CbxarticuloActionPerformed(evt);
            }
        });

        jLabel2.setText("Precio u/n ($):");

        jLabel3.setText("Total($):");

        jLabel4.setText("Cantidad:");

        txtcantidad.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtcantidadKeyReleased(evt);
            }
        });

        jLabel5.setText("Comprobante: ");

        jComboBox2.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "--Seleccione--", "Factura", "Recibo", "Remisión" }));

        jLabel6.setText("Número:");

        jLabel7.setText("Tipo Ingreso:");

        jComboBox3.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "--Seleccione--", "Compra", "Consumo interno ", "Ajuste inventario ", "Otro" }));

        jLabel8.setText("Proveedor:");

        Cboxproveedor.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        Cboxproveedor.setComponentPopupMenu(jPopupMenu1);

        jLabel10.setText("Información Factura");

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/iconfinder_add1-_32378.png"))); // NOI18N
        jButton1.setText("Nuevo");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/iconfinder_Save_32548.png"))); // NOI18N
        jButton2.setText("Guardar");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/if_button_cancel_1939.png"))); // NOI18N
        jButton3.setText("Eliminar");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel10)
                    .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 313, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel5)
                            .addComponent(jLabel8)
                            .addComponent(jLabel7)
                            .addComponent(jLabel6))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jComboBox2, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtnumero, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jComboBox3, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(Cboxproveedor, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel1)
                            .addComponent(jLabel3)
                            .addComponent(jLabel4))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(Cbxarticulo, javax.swing.GroupLayout.PREFERRED_SIZE, 206, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(txtcantidad, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 151, Short.MAX_VALUE)
                                    .addComponent(txtprecioun, javax.swing.GroupLayout.Alignment.LEADING))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lbmedida, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(lbmedida1, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(Lbtotal, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jButton3)))
                .addContainerGap(26, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(Cbxarticulo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(txtprecioun, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lbmedida, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(txtcantidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(lbmedida1, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel3)
                    .addComponent(Lbtotal, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(31, 31, 31)
                .addComponent(jLabel10)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(txtnumero, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(jComboBox3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(24, 24, 24)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(Cboxproveedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton3))
                .addGap(21, 21, 21))
        );

        tablaarticuloCompra.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tablaarticuloCompra);

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel9.setText("Nueva Compra");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator2)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 586, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel9)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 553, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void Articulos_CompraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Articulos_CompraActionPerformed
        // TODO add your handling code here:
        llenararticuloscompra();
    }//GEN-LAST:event_Articulos_CompraActionPerformed

    private void proveedor_compraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_proveedor_compraActionPerformed
        // TODO add your handling code here:
        llenarproveedorcompra();
    }//GEN-LAST:event_proveedor_compraActionPerformed

    private void txtcantidadKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtcantidadKeyReleased
        // TODO add your handling code here:
        if(txtcantidad.getText().equals("")){
        int total=Integer.parseInt(txtprecioun.getText())*0;
        Lbtotal.setText(String.valueOf(total));
        }else{
        int total=Integer.parseInt(txtprecioun.getText())*Integer.parseInt(txtcantidad.getText());
        Lbtotal.setText(String.valueOf(total));
        }
    }//GEN-LAST:event_txtcantidadKeyReleased

    private void CbxarticuloMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_CbxarticuloMouseClicked
        // TODO add your handling code here:
      
       
       
    }//GEN-LAST:event_CbxarticuloMouseClicked

    private void CbxarticuloActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CbxarticuloActionPerformed
        // TODO add your handling code here:
        //Items();
//        if(Cbxarticulo.getSelectedItem().equals(null)){}else{
//        String art=Cbxarticulo.getSelectedItem().toString(); }
    }//GEN-LAST:event_CbxarticuloActionPerformed

    private void CbxarticuloItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_CbxarticuloItemStateChanged
        // TODO add your handling code here:
      
      Items();
    }//GEN-LAST:event_CbxarticuloItemStateChanged

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
         guardarcompra();
         MostrarDatostabla();
         ActStock.ValidacionStockProductos(); //  actualizar el estado del inventario tras realizar una compra
         Venta.Acttablaproductoventa.doClick(); // actulizar los productos mostrados en la tabla de ventas
         int resp = JOptionPane.showConfirmDialog(null, "¿Desea agregar más artículo a su factura?", "Alerta!", JOptionPane.OK_CANCEL_OPTION);

            if(resp==0){
                txtprecioun.setText("");
                txtcantidad.setText("");
                Lbtotal.setText("");                
            }else{
                Limpiar();
            }
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
       Limpiar();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
      
    }//GEN-LAST:event_jButton3ActionPerformed
public void Items(){
   //Cbxarticulo.removeAllItems();
       String art=(String)Cbxarticulo.getSelectedItem();          
        
            String busqueda="";
        try {
            String SQL="SELECT * FROM articulo WHERE(nombre='"+art+"')";
     
            java.sql.Statement st= con.createStatement();
            
            ResultSet rs = st.executeQuery(SQL);
            rs.next();
            busqueda=rs.getString("medida");         
             
            
       
        } catch (Exception e) {
            //JOptionPane.showMessageDialog(null,"Error al combox" + e.getMessage());
        }
            if(busqueda == ""){
            //JOptionPane.showMessageDialog(null,"Error al combox" + e.getMessage());
            }else{
            lbmedida.setText(busqueda);
            lbmedida1.setText(busqueda);
            }
}

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JMenuItem Articulos_Compra;
    public static javax.swing.JComboBox<String> Cboxproveedor;
    public static javax.swing.JComboBox<String> Cbxarticulo;
    private javax.swing.JLabel Lbtotal;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JComboBox<String> jComboBox2;
    private javax.swing.JComboBox<String> jComboBox3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    public static javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JLabel lbmedida;
    private javax.swing.JLabel lbmedida1;
    public static javax.swing.JMenuItem proveedor_compra;
    private javax.swing.JTable tablaarticuloCompra;
    private javax.swing.JTextField txtcantidad;
    private javax.swing.JTextField txtnumero;
    private javax.swing.JTextField txtprecioun;
    // End of variables declaration//GEN-END:variables
}
