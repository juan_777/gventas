/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestiondeventas;

import com.mysql.jdbc.Connection;
import java.sql.*;
import javax.swing.JOptionPane;


/**
 *
 * @author Daniel
 */
public class Conexion_SQL {
    
    Connection conectar = null;
    
    public Connection conexion(){
        
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conectar = (Connection) DriverManager.getConnection("jdbc:mysql://localhost/gestorventas","root","");
            //JOptionPane.showMessageDialog(null,"Se genero la conexion con la base de datos");
            
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,"Se presento un error en la conexion con la base de datos"+e.getMessage());
        }
        
        return conectar;
    }
    
}
